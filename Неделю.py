from tkinter import *
from subprocess import call
import calendar
import datetime


def day_event(event):
    call(["python", "Календарь.py"])


def on_enter(e):
    info_label['fg'] = 'green'


def on_leave(e):
    info_label['fg'] = 'blue'


def clickMec(event):
    call(["python", "Календарь.py"])


def fill():
    info_label['text'] = calendar.month_name[month] + ', ' + str(year)
    weekdaytoday = datetime.datetime.today().weekday() #номер дня недели
    current_date = datetime.datetime.today() #текущая дата
    monday_date = current_date - datetime.timedelta(days=weekdaytoday) #получили поденельник текущей недели
    date_iterator = monday_date

    for j in range(7):
        days[j]['text'] = date_iterator.day
        days[j]['fg'] = 'black'
        date_iterator = date_iterator + datetime.timedelta(days=1)
        j = j + 1


root = Tk()
root.title('A week')
days = []
now = datetime.datetime.now()
year = now.year
month = now.month
info_label = Label(root, text='0', width=1, height=1, font='Arial 16 bold', fg='blue')
info_label.grid(row=0, column=1, columnspan=5, sticky=NSEW)
info_label.bind("<Button-1>",clickMec)
info_label.bind("<Enter>",on_enter)
info_label.bind("<Leave>",on_leave)

for n in range(7):
    lbl = Label(root, text=calendar.day_abbr[n], width=1, height=1, font='Arial 10 bold', fg='darkblue')
    lbl.grid(row=1, column=n, sticky=NSEW)

for row in range(1):
    for col in range(7):
        lbl = Label(root, text='0', width=4, height=2, font='Arial 16 bold',cursor='hand2')
        lbl.grid(row=row + 2, column=col, sticky=NSEW)
        lbl.bind("<Button-1>", day_event)
        days.append(lbl)

fill()
root.mainloop()
