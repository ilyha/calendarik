import tkinter as tk
import matplotlib.pyplot as plt
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from subprocess import call
import sqlite3


def exit_day():
    answer = messagebox.askokcancel('Выход', 'Вы точно хотите выйти?')
    if answer:
        root.destroy()

def open_fail():
    file_eating = "День.py"
    call(["python",file_eating])



class Main(tk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.init_main()
        self.db = db
        self.view_records()

    def init_main(self):
        toolbar = tk.Frame(bg='#d7d8e0', bd=2)
        toolbar.pack(side=tk.TOP, fill=tk.X)

        self.add_img = tk.PhotoImage(file="image/add.gif")
        btn_open_dialog = tk.Button(toolbar, text='Добавить', command=self.open_dialog, bg='#d7d8e0', bd=0,
        compound=tk.TOP, image=self.add_img)
        btn_open_dialog.pack(side=tk.LEFT)

        self.urdate_img = tk.PhotoImage(file="image/update.gif")
        btn_edit_dialog = tk.Button(toolbar, text='Редактировать', bg='#d7d8e0', bd=0, image=self.urdate_img,
                                    compound=tk.TOP, command=self.open_update_dialog)
        btn_edit_dialog.pack(side=tk.LEFT)

        self.delete_img = tk.PhotoImage(file="image/delete.gif")
        btn_delete = tk.Button(toolbar, text='Удалить', bg='#d7d8e0', bd=0, image=self.delete_img,
                                    compound=tk.TOP, command=self.delete_records)
        btn_delete.pack(side=tk.LEFT)

        self.search_img = tk.PhotoImage(file="image/search.gif")
        btn_search = tk.Button(toolbar, text='Поиск', bg='#d7d8e0', bd=0, image=self.search_img,
                               compound=tk.TOP, command=self.open_search_dialog)
        btn_search.pack(side=tk.LEFT)

        self.refresh_img = tk.PhotoImage(file="image/refresh.gif")
        btn_refresh = tk.Button(toolbar, text='Обновить', bg='#d7d8e0', bd=0, image=self.refresh_img,
                               compound=tk.TOP, command=self.view_records)
        btn_refresh.pack(side=tk.LEFT)

        main_menu = Menu(root)

        # Построение графика
        file_menu = Menu(main_menu, tearoff=0)
        file_menu.add_command(label='День', command=open_fail)
        file_menu.add_separator()
        file_menu.add_command(label='Построить график!', command=self.open_plotting_dialog)
        file_menu.add_separator()
        file_menu.add_command(label='Закрыть', command=exit_day)
        root.config(menu=file_menu)

        main_menu.add_cascade(label='Файл', menu=file_menu)

        root.config(menu=main_menu)
        ###
        self.tree = ttk.Treeview(self, columns=('ID', 'insulin', 'sugar_skinny', 'sugar_a_e', 'bread_ed', 'id_day'), height=15, show='headings')

        self.tree.column("ID", width=30, anchor=tk.CENTER)
        self.tree.column("insulin", width=100, anchor=tk.CENTER)
        self.tree.column("sugar_skinny", width=100, anchor=tk.CENTER)
        self.tree.column("sugar_a_e", width=100, anchor=tk.CENTER)
        self.tree.column("bread_ed", width=100, anchor=tk.CENTER)
        self.tree.column("id_day", width=100, anchor=tk.CENTER)

        self.tree.heading("ID", text='ID')
        self.tree.heading("insulin", text='Инсулин')
        self.tree.heading("sugar_skinny", text='Сахар на тощак')
        self.tree.heading("sugar_a_e", text='После еды')
        self.tree.heading("bread_ed", text='Хлебные ед.')
        self.tree.heading("id_day", text='Id_day')

        self.tree.pack(side=tk.LEFT)

        scroll = tk.Scrollbar(self, command=self.tree.yview)
        scroll.pack(side=tk.LEFT, fill=tk.Y)
        self.tree.configure(yscrollcommand=scroll.set)

    def records(self, insulin,sugar_skinny,sugar_a_e,bread_ed, id_day):
        self.db.insert_data(insulin,sugar_skinny,sugar_a_e,bread_ed, id_day)
        self.view_records()

    def update_record(self, insulin,sugar_skinny,sugar_a_e,bread_ed):
        self.db.c.execute('''UPDATE Eating SET insulin=?,sugar_skinny=?, sugar_after_eating=?,bread_ed=? WHERE ID=?''',
                          (insulin,sugar_skinny,sugar_a_e,bread_ed, self.tree.set(self.tree.selection()[0], '#1')))
        self.db.conn.commit()
        self.view_records()

    def view_records(self):
        self.db.c.execute('''SELECT * FROM Eating''')
        [self.tree.delete(i) for i in self.tree.get_children()]
        [self.tree.insert('', 'end', values = row) for row in self.db.c.fetchall()]

    def delete_records(self):
        for selection_item in self.tree.selection():
            self.db.c.execute('''DELETE FROM Eating WHERE ID=?''', (self.tree.set(selection_item, '#1'),))
        self.db.conn.commit()
        self.view_records()

    def search_records(self, insulin):
        insulin = ('%' + insulin + '%',)
        self.db.c.execute('''SELECT * FROM Eating WHERE insulin LIKE ? ''', insulin)
        [self.tree.delete(i) for i in self.tree.get_children()]
        [self.tree.insert('', 'end',values=row) for row in self.db.c.fetchall()]

    def open_dialog(self):
        Child()

    def open_update_dialog(self):
        Update()

    def open_search_dialog(self):
        Search()

    def open_plotting_dialog(self):
        self.db.c.execute('''SELECT insulin FROM Eating''')
        self.db.c.fetchall

        Insulin = []
        Name = ['Ytro','Obed','Yjin']

        for i in self.db.c:
            Insulin.append(i[0])

        plt.bar(Name,Insulin)
        plt.ylim(0, 10)
        plt.xlabel("Sugar scale")
        plt.ylabel("Reception time")
        plt.title("Inculin Information")
        plt.show()

class Child(tk.Toplevel):
    def __init__(self):
        super().__init__(root)
        self.init_child()
        self.view = app

    def init_child(self):
        self.title('Add eating')
        self.geometry('500x220+400+300')
        self.resizable(False, False)

        label_morning = tk.Label(self, text='Инсулин:')
        label_morning.place(x=20, y=20)
        label_morning_skinny = tk.Label(self, text='Сахар на тощак:')
        label_morning_skinny.place(x=20, y=40)
        label_lunch = tk.Label(self, text='Сахар после еды:')
        label_lunch.place(x=20, y=60)
        label_lunch_skinny = tk.Label(self, text='Хлебные ед.:')
        label_lunch_skinny.place(x=20, y=80)
        label_supper = tk.Label(self, text='Id_day:')
        label_supper.place(x=20, y=100)


        self.entry_morning = ttk.Entry(self)
        self.entry_morning.place(x=200, y=20)

        self.entry_morning_skinny = ttk.Entry(self)
        self.entry_morning_skinny.place(x=200, y=40)

        self.entry_lunch = ttk.Entry(self)
        self.entry_lunch.place(x=200, y=60)

        self.entry_lunch_skinny = ttk.Entry(self)
        self.entry_lunch_skinny.place(x=200, y=80)

        self.entry_supper = ttk.Entry(self)
        self.entry_supper.place(x=200, y=100)

        btn_cancel = ttk.Button(self, text='Закрыть', command=self.destroy)
        btn_cancel.place(x=300, y=170)

        self.btn_ok = ttk.Button(self, text='Добавить')
        self.btn_ok.place(x=220, y=170)
        self.btn_ok.bind(
            '<Button-1>', lambda event: self.view.records(
                self.entry_morning.get(),
                self.entry_morning_skinny.get(),
                self.entry_lunch.get(),
                self.entry_lunch_skinny.get(),
                self.entry_supper.get()
            )
        )

        self.grab_set()
        self.focus_set()


class Update(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_edit()
        self.view = app
        self.db = db
        self.default_data()

    def init_edit(self):
        self.title('Редактировать')
        self.geometry('500x220+400+300')
        self.resizable(False, False)

        label_morning = tk.Label(self, text='Инсулин:')
        label_morning.place(x=20, y=20)
        label_morning_skinny = tk.Label(self, text='Сахар на тощак:')
        label_morning_skinny.place(x=20, y=40)
        label_lunch = tk.Label(self, text='Сахар после еды:')
        label_lunch.place(x=20, y=60)
        label_lunch_skinny = tk.Label(self, text='Хлебные ед.:')
        label_lunch_skinny.place(x=20, y=80)

        self.entry_morning = ttk.Entry(self)
        self.entry_morning.place(x=200, y=20)

        self.entry_morning_skinny = ttk.Entry(self)
        self.entry_morning_skinny.place(x=200, y=40)

        self.entry_lunch = ttk.Entry(self)
        self.entry_lunch.place(x=200, y=60)

        self.entry_lunch_skinny = ttk.Entry(self)
        self.entry_lunch_skinny.place(x=200, y=80)

        btn_cancel = ttk.Button(self, text='Закрыть', command=self.destroy)
        btn_cancel.place(x=300, y=170)

        self.title('Редактировать')
        btn_edit = ttk.Button(self, text='Редактировать')
        btn_edit.place(x=205, y=170)
        btn_edit.bind('<Button-1>', lambda event: self.view.update_record(self.entry_morning.get(),
                                                                          self.entry_morning_skinny.get(),
                                                                          self.entry_lunch.get(),
                                                                          self.entry_lunch_skinny.get()))


    def default_data(self):
        self.db.c.execute('''SELECT * FROM Eating WHERE ID=?''', (self.view.tree.set(self.view.tree.selection()[0], '#1'),))
        row = self.db.c.fetchone()
        self.entry_morning.insert(0, row[1])
        self.entry_morning_skinny.insert(0, row[2])
        self.entry_lunch.insert(0, row[3])
        self.entry_lunch_skinny.insert(0, row[4])

class Search(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_search()
        self.view = app

    def init_search(self):
        self.title('Поиск')
        self.geometry('300x100+400+300')
        self.resizable(False, False)

        label_search = tk.Label(self, text='Поиск')
        label_search.place(x=50, y=20)

        self.entry_search = ttk.Entry(self)
        self.entry_search.place(x=105, y=20, width=150)

        bth_cancel = ttk.Button(self, text='Закрыть', command=self.destroy)
        bth_cancel.place(x=185, y=50)

        btn_search = ttk.Button(self, text='Поиск')
        btn_search.place(x=105, y=50)
        btn_search.bind('<Button-1>', lambda event: self.view.search_records(self.entry_search.get()))
        btn_search.bind('<Button-1>', lambda event: self.destroy(), add='+')

class DB:
    def __init__(self):
        self.conn = sqlite3.connect('Diabetes.db')
        self.c = self.conn.cursor()
        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS Eating (id INTEGER PRIMARY KEY, insulin REAL, sugar_skinny REAL, sugar_after_eating REAL, bread_ed REAL, id_day INTEGER, FOREIGN KEY (id_day) REFERENCES Day (id_day)
        ) ''')
        self.conn.commit()
    def insert_data(self, insulin,sugar_skinny,sugar_after_eating,bread_ed, id_day):
        self.c.execute('''INSERT INTO Eating (insulin,sugar_skinny,sugar_after_eating,bread_ed, id_day ) VALUES (?, ?, ?, ?, ?)''',
                       (insulin,sugar_skinny,sugar_after_eating,bread_ed, id_day))
        self.conn.commit()

if __name__ == "__main__":
    root = tk.Tk()
    db = DB()
    app = Main(root)
    app.pack()
    root.title("Eating")
    root.geometry("800x450+300+200")
    root.resizable(False, False)



    root.mainloop()