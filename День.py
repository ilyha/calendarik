import tkinter as tk
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from subprocess import call
import sqlite3

def exit_day():
    answer = messagebox.askokcancel('Выход', 'Вы точно хотите выйти?')
    if answer:
        root.destroy()

def open_fail():
    file_eating = "BloodSugar.py"
    call(["python",file_eating])

class Main(tk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.init_main()
        self.db = db
        self.view_records()

    def init_main(self):
        toolbar = tk.Frame(bg='#d7d8e0', bd=2)
        toolbar.pack(side=tk.TOP, fill=tk.X)

        self.add_img = tk.PhotoImage(file="image/add.gif")
        btn_open_dialog = tk.Button(toolbar, text='Добавить', command=self.open_dialog, bg='#d7d8e0', bd=0,
        compound=tk.TOP, image=self.add_img)
        btn_open_dialog.pack(side=tk.LEFT)

        self.urdate_img = tk.PhotoImage(file="image/update.gif")
        btn_edit_dialog = tk.Button(toolbar, text='Редактировать', bg='#d7d8e0', bd=0, image=self.urdate_img,
                                    compound=tk.TOP, command=self.open_update_dialog)
        btn_edit_dialog.pack(side=tk.LEFT)

        self.delete_img = tk.PhotoImage(file="image/delete.gif")
        btn_delete = tk.Button(toolbar, text='Удалить', bg='#d7d8e0', bd=0, image=self.delete_img,
                                    compound=tk.TOP, command=self.delete_records)
        btn_delete.pack(side=tk.LEFT)

        self.search_img = tk.PhotoImage(file="image/search.gif")
        btn_search = tk.Button(toolbar, text='Поиск', bg='#d7d8e0', bd=0, image=self.search_img,
                               compound=tk.TOP, command=self.open_search_dialog)
        btn_search.pack(side=tk.LEFT)

        self.refresh_img = tk.PhotoImage(file="image/refresh.gif")
        btn_refresh = tk.Button(toolbar, text='Обновить', bg='#d7d8e0', bd=0, image=self.refresh_img,
                               compound=tk.TOP, command=self.view_records)
        btn_refresh.pack(side=tk.LEFT)

        self.tree = ttk.Treeview(self, columns=('id_day', 'data', 'id_usera'), height=15, show='headings')

        self.tree.column("id_day", width=30, anchor=tk.CENTER)
        self.tree.column("data", width=365, anchor=tk.CENTER)
        self.tree.column("id_usera", width=150, anchor=tk.CENTER)

        self.tree.heading("id_day", text='Id_day')
        self.tree.heading("data", text='Data')
        self.tree.heading("id_usera", text='Id_usera')

        self.tree.pack(side=tk.LEFT)

        scroll = tk.Scrollbar(self, command=self.tree.yview)
        scroll.pack(side=tk.LEFT, fill=tk.Y)
        self.tree.configure(yscrollcommand=scroll.set)

    def records(self, data, id_usera):
        self.db.insert_data(data, id_usera)
        self.view_records()

    def update_record(self, data,id_usera):
        self.db.c.execute('''UPDATE Day SET data=?,id_usera=? WHERE id_day=?''',
                          (data,id_usera, self.tree.set(self.tree.selection()[0], '#1')))
        self.db.conn.commit()
        self.view_records()

    def view_records(self):
        self.db.c.execute('''SELECT * FROM Day ''')
        [self.tree.delete(i) for i in self.tree.get_children()]
        [self.tree.insert('', 'end', values = row) for row in self.db.c.fetchall()]

    def delete_records(self):
        for selection_item in self.tree.selection():
            self.db.c.execute('''DELETE FROM Day WHERE id_day=?''', (self.tree.set(selection_item, '#1'),))
        self.db.conn.commit()
        self.view_records()

    def search_records(self, id_day):
        id_day = ('%' + id_day + '%',)
        self.db.c.execute('''SELECT * FROM day WHERE id_day LIKE ? ''', id_day)
        [self.tree.delete(i) for i in self.tree.get_children()]
        [self.tree.insert('', 'end',values=row) for row in self.db.c.fetchall()]


    def open_dialog(self):
        Child()

    def open_update_dialog(self):
        Update()

    def open_search_dialog(self):
        Search()

class Child(tk.Toplevel):
    def __init__(self):
        super().__init__(root)
        self.init_child()
        self.view = app

    def init_child(self):
        self.title('Add insulin')
        self.geometry('400x220+400+300')
        self.resizable(False, False)

        label_morning = tk.Label(self, text='Дата:')
        label_morning.place(x=50, y=50)
        label_lunch = tk.Label(self, text='Id usera:')
        label_lunch.place(x=50, y=80)

        self.entry_morning = ttk.Entry(self)
        self.entry_morning.place(x=200, y=50)
        self.entry_lunch = ttk.Entry(self)
        self.entry_lunch.place(x=200, y=80)


        btn_cancel = ttk.Button(self, text='Закрыть', command=self.destroy)
        btn_cancel.place(x=300, y=170)

        self.btn_ok = ttk.Button(self, text='Добавить')
        self.btn_ok.place(x=220, y=170)
        self.btn_ok.bind('<Button-1>', lambda event: self.view.records(self.entry_morning.get(),
                                                                  self.entry_lunch.get()))

        self.grab_set()
        self.focus_set()

class Update(Child):
    def __init__(self):
        super().__init__()
        self.init_edit()
        self.view = app
        self.db = db
        self.default_data()

    def init_edit(self):
        self.title('Редактировать')
        btn_edit = ttk.Button(self, text='Редактировать')
        btn_edit.place(x=205, y=170)
        btn_edit.bind('<Button-1>', lambda event: self.view.update_record(self.entry_morning.get(),
                                                                  self.entry_lunch.get()))
        self.btn_ok.destroy()

    def default_data(self):
        self.db.c.execute('''SELECT * FROM Day WHERE id_day=?''', (self.view.tree.set(self.view.tree.selection()[0], '#1'),))
        row = self.db.c.fetchone()
        self.entry_morning.insert(0, row[1])



class Search(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_search()
        self.view = app

    def init_search(self):
        self.title('Поиск')
        self.geometry('300x100+400+300')
        self.resizable(False, False)

        label_search = tk.Label(self, text='Поиск')
        label_search.place(x=50, y=20)

        self.entry_search = ttk.Entry(self)
        self.entry_search.place(x=105, y=20, width=150)

        bth_cancel = ttk.Button(self, text='Закрыть', command=self.destroy)
        bth_cancel.place(x=185, y=50)

        btn_search = ttk.Button(self, text='Поиск')
        btn_search.place(x=105, y=50)
        btn_search.bind('<Button-1>', lambda event: self.view.search_records(self.entry_search.get()))
        btn_search.bind('<Button-1>', lambda event: self.destroy(), add='+')



class DB:
    def __init__(self):
        self.conn = sqlite3.connect('Diabetes.db')
        self.c = self.conn.cursor()
        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS Day (id_day integer primary key, data text, id_usera integer, FOREIGN KEY(id_usera) REFERENCES users(id_usera))''')
        self.conn.commit()
    def insert_data(self, data, id_usera):
        self.c.execute('''INSERT INTO Day(data,id_usera) VALUES (?,?)''',
                       (data, id_usera))
        self.conn.commit()

if __name__ == "__main__":
    root = tk.Tk()
    db = DB()
    app = Main(root)
    app.pack()
    root.title("Day")
    root.geometry("665x450+300+200")
    root.resizable(False, False)

    main_menu = Menu(root)

    #Файл
    file_menu = Menu(main_menu,tearoff=0)
    file_menu.add_command(label='Прием пиши', command=open_fail)
    file_menu.add_separator()
    file_menu.add_command(label='Закрыть', command=exit_day)
    root.config(menu=file_menu)

    main_menu.add_cascade(label='Файл', menu=file_menu)

    root.config(menu=main_menu)

    root.mainloop()