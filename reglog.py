### Авторизация
import sqlite3
import tkinter as tk
from subprocess import call
from tkinter import *
from tkinter import  messagebox

root = Tk()
root.title('Авторизация')
root.geometry("450x400+300+200")
root.configure(bg="#fff")
root.resizable(False, False)
db = sqlite3.connect('Diabetes.db')
cursor = db.cursor()

def click_reg():
    call(["python", "Registration.py"])

def signin():
    username = user.get()
    password = passw.get()

    if username != '' and password != '':
        if len(username) == 0:
            return
        if len(password) == 0:
            return
        cursor.execute(f'SELECT login FROM users WHERE login="{username}"')
        check_login = cursor.fetchall()
        cursor.execute(f'SELECT password FROM users WHERE login="{username}"')
        check_pass = cursor.fetchall()

        if check_pass[0][0] == password and check_login[0][0] == username:
            messagebox.showinfo('Упешно', 'Успешная авторизация!')
            call(["python", "Календарь.py"])
        else:
            messagebox.showerror('Ошибка', 'Ошибка авторизации!')
    else:
        messagebox.showerror('Ошибка', 'Не введены данные!')

frame=Frame(root,width=320, height=320, bg='blue')
frame.place(relx=0.15, rely=0.15)
heading = Label(frame,text='Авторизация',fg='#57a1f8', bg='blue',font=('Microsoft YaHei UI Light', 18,'bold'))
heading.place(x=75, y=15)
#######------------------------------------------------------------------------------------------

heading = Label(frame, text = 'Логин', bg = 'blue', font = ('Microsoft YaHei UI Light', 14, 'bold' ))
heading.place(x = 30, y = 50)
user = Entry(frame, width=23,fg='black',border=0,bg="white", font=('Microsoft YaHei UI Light', 11))
user.place(x=30,y=80)


#######-----------------------------------------------------------------------------------------

def toggle_password():
    if passw.cget('show') == '':
        passw.config(show='*')
        c1.config(text='Показать')
    else:
        passw.config(show='')
        c1.config(text='Скрыть')

c1 = tk.Checkbutton(root, text='Показать', command=toggle_password)
c1.place(x = 300, y = 205)

heading = Label(frame, text = 'Пароль', bg = 'blue', font = ('Microsoft YaHei UI Light', 14, 'bold' ))
heading.place(x = 30, y = 120)
passw = Entry(frame,width=23,fg='black',border=0,bg="white", font=('Microsoft YaHei UI Light', 11))
passw.place(x=30,y=150)


################################################################################################

Button(frame,width=30,pady=7,text='Войти', bg='#57a1f8', fg='white', border=0, cursor='hand2', command=signin).place(x=50, y=204)
### Кнопка перехода на окно регистрация
sing_up = Button(frame, width=15, text='Регистрация', border=0, bg='white', cursor='hand2', fg='#57a1f8', command=click_reg)
sing_up.place(x=105,y=270)

root.mainloop()

