# Import Required Library
from tkinter import *
from tkcalendar import Calendar

import datetime
import sqlite3
from subprocess import call

connection = sqlite3.connect('Diabetes.db',
                             detect_types=sqlite3.PARSE_DECLTYPES |
                             sqlite3.PARSE_COLNAMES)
cursor = connection.cursor()


# Create Object
root = Tk()

# Set geometry
root.geometry("400x400")

# Add Calendar
cal = Calendar(root, selectmode='day', date_pattern="dd-mm-y")

cal.pack(pady=70)

# method to add days
def add_days():
    date_1 = cal.get_date()
    #cursor.execute(f'INSERT INTO Day(data) VALUES ("{date_1}")')
    #connection.commit()
    call(["python", "День.py"])
    print(date_1)

frame2 = Frame()
frame2.pack()

# making buttons
Button(frame2, text="Add Days",
       command=add_days, font="italic 15").pack(side=LEFT)

# Execute Tkinter
root.mainloop()
