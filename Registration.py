### Регистрация
import tkinter as tk
from tkinter import *
from tkinter import messagebox
from subprocess import call
import sqlite3

window=Tk()
window.title('Регистрация')
window.geometry('450x500+300+200')
window.configure(bg='#fff')
window.resizable(False,False)

db = sqlite3.connect('Diabetes.db')
cursor = db.cursor()

cursor.execute('''CREATE TABLE IF NOT EXISTS users (id_usera integer primary key,login TEXT, password TEXT)''')
db.commit()

def click_week():
    call(["python", "reglog.py"])

def signup():
    username = user.get()
    password = passw.get()
    password_repeat = passw_repeat.get()

    if username!='' and password!='' and password_repeat!='':
        if len(username) == 0:
            return
        if len(password) == 0:
            return
        if len(password_repeat) == 0:
            return
        if password == password_repeat:
            cursor.execute(f'SELECT login FROM users WHERE login="{username}"')
            if cursor.fetchone() is None:
                cursor.execute(f'INSERT INTO users(login,password) VALUES ("{username}","{password}")')
                messagebox.showinfo('Успешно', f'Аккаунт {username} успешно зарегистрирован!')
                db.commit()
                call(["python", "Календарь.py"])
            else:
                messagebox.showerror('Ошибка', 'Такая запись уже имеется!')
        else:
            messagebox.showerror('Ошибка', 'Пароли не совпадают!')
    else:
        messagebox.showerror('Ошибка', 'Не введены данные, заполните поля!')

frame = Frame(window, width=350, height=390, bg='blue')
frame.place(x=50, y=50)

heading = Label(frame,text='Регистрация',fg='#57a1f8', bg='blue',font=('Microsoft YaHei UI Light', 20,'bold'))
heading.place(x=100, y=20)

##################################
###Логин

heading = Label(frame, text = 'Логин', bg = 'blue', font = ('Microsoft YaHei UI Light', 16, 'bold' ))
heading.place(x = 30, y = 50)
user = Entry(frame,width=25,fg='black',border=2,bg="white", font=('Microsoft YaHei UI Light', 11))
user.place(x=30, y=80)



#######-----------------------------------------------------------------------------------------
###Пароль
def toggle_password():
    if passw.cget('show') == '':
        passw.config(show='*')
        c1.config(text='Показать')
    else:
        passw.config(show='')
        c1.config(text='Скрыть')

c1 = tk.Checkbutton(window, text='Показать', command=toggle_password)
c1.place(x = 300, y = 210)
heading = Label(frame, text = 'Пароль', bg = 'blue', font = ('Microsoft YaHei UI Light', 16, 'bold' ))
heading.place(x = 30, y = 120)
passw = Entry(frame,width=25,fg='black',border=0,show='*',bg="white", font=('Microsoft YaHei UI Light', 11))
passw.place(x=30,y=150)

#######-----------------------------------------------------------------------------------------
###Повторный пароль
def toggle_password2():
    if passw_repeat.cget('show') == '':
        passw_repeat.config(show='*')
        c2.config(text='Показать')
    else:
        passw_repeat.config(show='')
        c2.config(text='Скрыть')
heading = Label(frame, text = 'Повторите пароль', bg = 'blue', font = ('Microsoft YaHei UI Light', 16, 'bold' ))
heading.place(x = 30, y = 190)
passw_repeat = Entry(frame,width=25,fg='black',border=0,show='*',bg="white", font=('Microsoft YaHei UI Light', 11))
passw_repeat.place(x=30,y=220)

c2 = tk.Checkbutton(window, text='Показать', command=toggle_password2)
c2.place(x=300, y=270)

################################################################################################

Button(frame, width=39, pady=7, text='Зарегистрироваться', bg='#57a1f8', fg='white',cursor='hand2', border=0, command=signup).place(x=35, y=280)


signin=Button(frame, width=20, text='Авторизация', border=0, bg='white', cursor='hand2', fg='#57a1f8', command=click_week)
signin.place(x=105, y=340)
window.mainloop()